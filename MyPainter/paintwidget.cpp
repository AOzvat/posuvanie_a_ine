#include "paintwidget.h"


PaintWidget::PaintWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
	myPenWidth = 5;
	myPenColor = Qt::blue;
}

bool PaintWidget::openImage(const QString &fileName)
{
	QImage loadedImage;
	if (!loadedImage.load(fileName))
		return false;

	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::newImage(int x, int y)
{
	QImage loadedImage(x,y,QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::saveImage(const QString &fileName)
{
	QImage visibleImage = image;
	resizeImage(&visibleImage, size());

	if (visibleImage.save(fileName,"png")) {
		modified = false;
		return true;
	}
	else {
		return false;
	}
}

void PaintWidget::setPenColor(const QColor &newColor)
{
	myPenColor = newColor;
}

void PaintWidget::setPenWidth(int newWidth)
{
	myPenWidth = newWidth;
}

void PaintWidget::clearImage()
{
	image.fill(qRgb(255, 255, 255));
	modified = true;
	update();
}

void PaintWidget::mousePressEvent(QMouseEvent *event)
{
	//lavy klik mysi (Spir)
	if (event->button() == Qt::LeftButton) {
		lastPoint = event->pos();
		painting = true;
	}
	//pravy klik mysi (ja)
	else {
		dda();
		scan_line();
	}
}

void PaintWidget::mouseDoubleClickEvent(QMouseEvent *event) {
	//vykresli povodny objekt
	dda();

	//bool pre posuvanie
	if (posunutie == true) {
		posunutie = false;
	}
	else {
		posunutie = true;
	}
}

void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
	if ((event->buttons() & Qt::LeftButton) && painting)
		drawLineTo(event->pos());
}

void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && painting) {
		drawLineTo(event->pos());
		painting = false;
	}
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void PaintWidget::drawLineTo(const QPoint &endPoint)
{
	QPainter painter(&image);
	painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	modified = true;
	painter.drawPoint(lastPoint);

	//nacitaju sa body za posuvania objektu
	if (posunutie == true) {
		body_posunu.push_back(lastPoint);
	}
	//nacitaju sa body za objekt
	else {
		body.push_back(lastPoint);
	}

	update();
}

void PaintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

void PaintWidget::nastav_farbu_hranici(int red, int green, int blue, int a) {
	farba_hranici.setRgb(red, green, blue, a);
}

void PaintWidget::nastav_farbu_vyplne(int red, int green, int blue, int a) {
	farba_vyplne.setRgb(red, green, blue, a);
}

void PaintWidget::dda() {
	int x1, y1, x2, y2, dx, dy, krok;
	float zvacsene_x, zvacsene_y, x, y;

	QPainter painter(&image);
	painter.setPen(QPen(farba_hranici, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	
	//prejde vsetky body
	for (int i = 0; i < body.size(); i++) {
		//zaradi do premennych body
		if (i != body.size() - 1) {
			x1 = body[i].x();
			y1 = body[i].y();
			x2 = body[i + 1].x();
			y2 = body[i + 1].y();
		}
		else {
			x1 = body[i].x();
			y1 = body[i].y();
			x2 = body[0].x();
			y2 = body[0].y();
		}

		//vypocita rozdiel medzi bodmi
		dx = x2 - x1;
		dy = y2 - y1;

		//pre zaciatok pripadi zaciatocny bod
		x = x1;
		y = y1;

		//rozhodne sa v ktorom smere sa bude krokuvat
		if (abs(dx) > abs(dy)) {
			krok = abs(dx);
		}
		else {
			krok = abs(dy);
		}

		//o kolko sa budu suradnice zvacsovat
		// if dx > dy -> zvacsenie_x = 1
		// if dx < dy -> zvacsenie_y = 1
		zvacsene_x = dx / (float)krok;
		zvacsene_y = dy / (float)krok;

		//zvacsovanie a vykreslovanie "ciare"
		for (int j = 0; j < krok; j++) {
			x += zvacsene_x;
			y += zvacsene_y;
			painter.drawPoint(x, y);
		}
	}
	
	update();
}

void PaintWidget::bresen() {
	int x1, y1, x2, y2, dx, dy, dx1, dy1, krok;
	int x, y;
	float chyba;
	int k1, k2, p;

	QPainter painter(&image);
	painter.setPen(QPen(farba_hranici, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

	//prejde vsetky body
	for (int i = 0; i < body.size(); i++) {
		//zaradi do premennych body
		if (i != body.size() - 1) {
			x1 = body[i].x();
			y1 = body[i].y();
			x2 = body[i + 1].x();
			y2 = body[i + 1].y();
		}
		else {
			x1 = body[i].x();
			y1 = body[i].y();
			x2 = body[0].x();
			y2 = body[0].y();
		}

		//vypocita rozdiel medzi bodmi
		dx = x2 - x1;
		dy = y2 - y1;

		//aby sme kreslili vzdy iba ciaru do 45 stupnov, nie strmsiu
		if (abs(dy) > abs(dx)) {
			std::swap(x1, y1);
			std::swap(x2, y2);
		}

		//aby sme vzdy kreslili ciaru s lava na pravo
		if (x1 > x2) {
			std::swap(x1, x2);
			std::swap(y1, y2);
		}

		//vypocita rozdiel medzi vymenenymi bodmi
		dx1 = x2 - x1;
		dy1 = y2 - y1;

		chyba = dx1 / 2.0f;

		//alebo sa ciara bude kreslit dolu alebo hore vzhladom na y
		if (y1 < y2) {
			krok = 1;
		}
		else {
			krok = -1;
		}

		y = y1;

		//zvacsuje x vzdy o 1
		for (int x = x1; x < x2; x++) {
			//ak sa hore kvoli strmosti ciare zamenili x a y, teraz to vratime spat
			//kreslime y, x a ak nie, tak normalne x, y
			if (abs(dy) > abs(dx)) {
				painter.drawPoint(y, x);
			}
			else {
				painter.drawPoint(x, y);
			}

			chyba -= abs(dy1);
			if (chyba < 0) {
				y += krok;
				chyba += dx1;
			}
		}
	}

	update();
}

void PaintWidget::kresli_body(int pocet_body, double polomer) {
	int xs = image.width();
	int ys = image.height();
	QPoint tmp;

	QPainter painter(&image);
	painter.setPen(QPen(farba_hranici, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

	double delenia = 2.0 * M_PI / pocet_body;


	for (int i = 0; i < pocet_body; i++) {
		double uhol = delenia * i;

		int x = (int)((xs / 2) + (polomer * cos(uhol)));
		int y = (int)((ys / 2) + (polomer * sin(uhol)));

		tmp.setX(x);
		tmp.setY(y);

		body.push_back(tmp);

		painter.drawPoint(x, y);
	}
	update();
}

void PaintWidget::scan_line() {
	int x1, x2, y1, y2;
	int dx, dy;

	QPainter painter(&image);
	painter.setPen(QPen(farba_vyplne, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

	//struktura pre hrany, ktora obsahuje zaciatocny bod (x, y)
	//koncovi bod y, obratenu smernicu w a operator porovnania
	struct hrana {
		int x1;
		int y1;
		int y2;
		float w;
		bool operator< (const hrana &h) const {
			if (y1 != h.y1) {
				return y1 < h.y1;
			}

			if (y1 == h.y1) {
				if (x1 == h.x1) {
					return w < h.w;
				}
				else {
					return x1 < h.x1;
				}
			}
		}
	};

	//struktura aktivnich hran, obsahuje index aktivnej hrany
	//aktualne x a operator porovnania
	struct aktivna_hrana {
		int hrana_index;
		float xa;
		bool operator< (const aktivna_hrana &ah) const {
			return xa < ah.xa;
		}
	};

	int ymin = 0, ymax = 0;
	std::vector<hrana> hrany;
	hrana tmp;

	//prejde vsetky body
	for (int i = 0; i < body.size(); i++) {
		//zaradi do premennych body
		if (i != body.size() - 1) {
			x1 = body[i].x();
			y1 = body[i].y();
			x2 = body[i + 1].x();
			y2 = body[i + 1].y();
		}
		else {
			x1 = body[i].x();
			y1 = body[i].y();
			x2 = body[0].x();
			y2 = body[0].y();
		}

		//vodorovne hrany sa vyhodia
		if ((y2 - y1) != 0) {
			//aby vzdy bola z hora nadol hrana
			if (y1 > y2) {
				std::swap(x1, x2);
				std::swap(y1, y2);
			}

			//skratime hranicu o jeden pixel v y-osi
			y2 -= 1;

			//vypocita rozdiel medzi bodmi
			dx = x2 - x1;
			dy = y2 - y1;

			//vlozime hranu do tabulky hran
			tmp.x1 = x1;
			tmp.y1 = y1;
			tmp.y2 = y2;
			tmp.w = dx / (float)dy;

			hrany.push_back(tmp);

			//nastav ymax
			if (tmp.y2 > ymax) {
				ymax = tmp.y2;
			}
		}
	}

	//vysortujem tabulku hran
	std::sort(hrany.begin(), hrany.end());

	ymin = hrany[0].y1;
	std::vector<aktivna_hrana> aktivne_hrany;
	aktivna_hrana aktivna;

	//ScanLine
	for (int ya = ymin; ya <= ymax; ya++) {
		for (int i = 0; i < hrany.size(); i++) {
			if (ya == hrany[i].y1) {
				aktivna.hrana_index = i;
				aktivna.xa = hrany[i].w * ya - hrany[i].w * hrany[i].y1 + hrany[i].x1;

				aktivne_hrany.push_back(aktivna);
			}
		}

		std::sort(aktivne_hrany.begin(), aktivne_hrany.end());

		//vykreslovanie medzi
		for (int i = 0; i < aktivne_hrany.size() - 1; i++) {
			if ((int)(aktivne_hrany[i].xa + 0.5) != (int)(aktivne_hrany[i + 1].xa + 0.5)) {
				QPoint A((int)(aktivne_hrany[i].xa + 1.5), ya);
				QPoint B((int)(aktivne_hrany[i + 1].xa - 0.5), ya);
	
				for (int x = A.x(); x <= B.x(); x++) {
					painter.drawPoint(x, ya);
				}
			}

			aktivne_hrany[i].xa += hrany[aktivne_hrany[i].hrana_index].w;
			aktivne_hrany[i + 1].xa += hrany[aktivne_hrany[i + 1].hrana_index].w;

			if (i + 2 >= aktivne_hrany.size()) {
				break;
			}
			i++;
		}

		for (int i = 0; i < aktivne_hrany.size(); i++) {
			if (ya == hrany[aktivne_hrany[i].hrana_index].y2) {
				aktivne_hrany.erase(aktivne_hrany.begin() + i);
				i--;
			}
		}
	}

	update();
}

void PaintWidget::posun() {
	int px, py, x, y;

	//vypocita posun
	px = body_posunu[body_posunu.size() - 1].x() - body_posunu[0].x();
	py = body_posunu[body_posunu.size() - 1].y() - body_posunu[0].y();

	//vypocita nove hodnoty
	for (int i = 0; i < body.size(); i++) {
		x = (int)(body[i].x() + px);
		y = (int)(body[i].y() + py);
		body[i].setX(x);
		body[i].setY(y);
	}

	//vycisti platno a nakresli novy objekt
	dda();

	//vycisty vektor body_posunu
	body_posunu.clear();
}

void PaintWidget::otoc(float hodnota, bool smer) {
	int sx, sy, x = 0, y = 0;
	float hodnota_rad = 0;

	//prevedie stupne do riadianov
	hodnota_rad = hodnota * (M_PI / 180);

	//bod okolo ktoreho ideme otacat
	sx = body[0].x();
	sy = body[0].y();

	//proti smeru hodinovych ruciciek
	if (!smer) {
		for (int i = 0; i < body.size(); i++) {
			x = (int)((body[i].x() - sx) * cos(hodnota_rad) + (body[i].y() - sy) * sin(hodnota_rad) + sx);
			y = (int)(-(body[i].x() - sx) * sin(hodnota_rad) + (body[i].y() - sy) * cos(hodnota_rad) + sy);
			body[i].setX(x);
			body[i].setY(y);
		}
	}
	//v smere hodinovych ruciciek
	else {
		for (int i = 0; i < body.size(); i++) {
			x = (int)((body[i].x() - sx) * cos(hodnota_rad) - (body[i].y() - sy) * sin(hodnota_rad) + sx);
			y = (int)((body[i].x() - sx) * sin(hodnota_rad) + (body[i].y() - sy) * cos(hodnota_rad) + sy);
			body[i].setX(x);
			body[i].setY(y);
		}
	}

	//vycisti platno a nakresli novy objekt
	dda();
}

void PaintWidget::skaluj(float hodnota) {
	int sx, sy, x, y;

	//stred moj
	sx = body[0].x();
	sy = body[0].y();

	//vypocita nove hodnoty
	for (int i = 0; i < body.size(); i++) {
		//vypocitame vzdialenost od stredu
		x = body[i].x() - sx;
		y = body[i].y() - sy;

		//preskalujeme
		x = (int)(hodnota * x);
		y = (int)(hodnota * y);
		body[i].setX(x + sx);
		body[i].setY(y + sy);
	}

	//vycisti platno a nakresli novy objekt
	dda();
}

void PaintWidget::preklop_ciara() {
	QPainter painter(&image);
	painter.setPen(QPen(farba_hranici, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

	//spravy ciaru
	painter.drawLine(body[0].x(), 0, body[0].x(), image.height());
}

void PaintWidget::preklop() {
	int a, b, c, x, y;
	float d;

	//vypovet normaly
	//a = b1.y - b2.y
	//b = b2.x - b1.x
	a = body[0].y() - (body[0].y() - 1);
	b = body[0].x() - body[0].x();

	//dopocitame c
	c = -(a * body[0].x()) - (b * body[0].y());

	//vypocita nove hodnoty
	for (int i = 0; i < body.size(); i++) {
		//vypocet d
		d = ((a * body[i].x()) + (b * body[i].y()) + c) / ((a * a) + (b * b));

		x = (int)(body[i].x() - 2 * a * d);
		y = (int)(body[i].y() - 2 * b * d);
		body[i].setX(x);
		body[i].setY(y);
	}

	//vycisti platno a nakresli novy objekt
	dda();
}

void PaintWidget::skos(float hodnota, bool smer) {
	int sx, sy, x, y;

	//stred moj
	sx = body[0].x();
	sy = body[0].y();

	//v smere osi y
	if (smer) {
		for (int i = 0; i < body.size(); i++) {
			//vypocitame vzdialenost od stredu
			x = body[i].x() - sx;
			y = body[i].y() - sy;

			x = (int)(x - hodnota * y);
			body[i].setX(x + sx);
		}
	}
	//v smere osi x
	else {
		for (int i = 0; i < body.size(); i++) {
			//vypocitame vzdialenost od stredu
			x = body[i].x() - sx;
			y = body[i].y() - sy;

			y = (int)(y - hodnota * x);
			body[i].setY(y + sy);
		}
	}

	//vycisti platno a nakresli novy objekt
	dda();
}

void PaintWidget::vymaz_vsetko() {
	//ocisti vsetky vektory
	clearImage();
	body.clear();
	body_posunu.clear();

	posunutie= false;
}